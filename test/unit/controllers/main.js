'use strict';

suite('Controller: MainCtrl', function () {

  setup(function(done) {
    var self = this;

    module('angularBaseApp');

    inject(function ($controller, $rootScope) {
      self.$scope = $rootScope.$new();
      self.mainCtrl = $controller('MainCtrl', {
        $scope: self.$scope
      });
      done();
    })
  });


  test('should attach a list of awesomeThings to the scope', function () {
    assert.equal(3, this.$scope.awesomeThings.length);
  });
});
